const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const {authMiddleware} = require('./middleware/authMiddleware');


const {notesRouter} = require('./notesRouter.js');
const {usersRouter} = require('./usersRouter.js');
const {authRouter} = require('./authRouter.js');

const DB_URL = process.env.DB_URL;
const PORT = process.env.PORT;
const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use('/api/users', authMiddleware, usersRouter);

app.use('/api/notes', authMiddleware, notesRouter);


async function startApp() {
  try {
    await mongoose.connect(DB_URL);
    app.listen(PORT || 8080, () =>
      console.log('SERVER STARTED ON PORT ' + PORT));
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

startApp();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err.message);
  res.status(500).send({message: 'Server error'});
}

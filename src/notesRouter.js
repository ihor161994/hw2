const express = require('express');
const router = express.Router();
const {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  isCompletedNoteById,
  deleteNoteById,
} = require('./notesService.js');


router.get('/', getNotes);

router.post('/', addNote);

router.get('/:id', getNoteById);

router.put('/:id', updateNoteById);

router.patch('/:id', isCompletedNoteById);

router.delete('/:id', deleteNoteById);


module.exports = {
  notesRouter: router,
};

const {User} = require('./models/Users.js');
const bcrypt = require('bcryptjs');


const getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    const {id, username, createdAt} = user;
    return res.json({
      'user': {
        '_id': id,
        username,
        'createdDate': createdAt,
      },
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const deleteUser = async (req, res, next) => {
  try {
    await User.findByIdAndDelete(req.user.userId);
    return res.json({
      'message': 'Success',
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const changePasswordUser = async (req, res, next) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const user = await User.findById(req.user.userId);

    if (oldPassword &&
            await bcrypt.compare(String(oldPassword), String(user.password))) {
      if (newPassword) {
        user.password = await bcrypt.hash(newPassword, 10);
        user.save()
            .then(res.json({
              'message': 'Success',
            }))
            .catch((err) => {
              next(err);
            });
      } else {
        return res.status(400).json({
          'message': 'Invalid new password',
        });
      }
    } else {
      return res.status(400).json({
        'message': 'Invalid old password',
      });
    }
  } catch
  (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

module.exports = {
  getUser,
  deleteUser,
  changePasswordUser,
};

const {User} = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();

const key = process.env.SECRET_KEY;

const registerUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;

    if (!username) {
      return res.status(400).send({
        message: 'Username field is required',
      });
    }

    if (!password) {
      return res.status(400).send({
        message: 'Password field is required',
      });
    }

    const candidate = await User.findOne({username});
    if (candidate) {
      return res.status(400).json({
        'message': `Username '${username}' already exists`,
      });
    }

    const hashPassword = bcrypt.hashSync(password, 10);
    await User.create({username, password: hashPassword});
    res.json({
      message: 'Success',
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const loginUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;

    if (!username) {
      return res.status(400).send({
        message: 'Username field is required',
      });
    }

    if (!password) {
      return res.status(400).send({
        message: 'Password field is required',
      });
    }

    const user = await User.findOne({username: username});

    if (!user) {
      return res.status(400).json({
        'message': `Username '${username}' not found`,
      });
    }

    const isCompare = bcrypt.compareSync(String(password),
        String(user.password));

    if (isCompare) {
      const payload = {userId: user._id};
      const token = jwt.sign(payload, key);
      return res.json({
        message: 'Success',
        jwt_token: token,
      });
    }
    return res.status(400).json({
      'message': 'Password is not valid',
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

module.exports = {
  registerUser,
  loginUser,
};

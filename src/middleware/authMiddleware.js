const jwt = require('jsonwebtoken');
require('dotenv').config();
const {User} = require('../models/Users.js');


const key = process.env.SECRET_KEY;

const authMiddleware = async (req, res, next) => {
  const {
    authorization,
  } = req.headers;


  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(400).json({
      'message': 'User not authorization',
    });
  }

  try {
    const tokenPayload = jwt.verify(token, key);

    const user = await User.findById(tokenPayload.userId);

    if (!user) {
      return res.status(400).json({message: 'User not found'});
    }

    req.user = tokenPayload;
    next();
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

module.exports = {
  authMiddleware,
};

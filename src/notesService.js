const {Note} = require('./models/Notes.js');


const getNotes = async (req, res, next) => {
  try {
    let notes = await Note.find({userId: req.user.userId}, '-__v');
    const count = notes.length;

    let {offset, limit} = req.query;

    if (offset) {
      notes = notes.filter((note, index) => index >= offset - 1);
    }
    if (limit) {
      notes = notes.filter((note, index) => index <= limit - 1);
    }

    offset = !!offset ? offset : 0;
    limit = !!limit ? limit : 0;


    return res.json({
      'offset': Number(offset),
      'limit': Number(limit),
      'count': count,
      'notes': notes,
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const addNote = async (req, res, next) => {
  try {
    const {text} = req.body;

    if (!text) {
      return res.status(400).json({
        'message': 'Text field is required',
      });
    }

    const {userId} = req.user;
    const note = new Note({
      userId,
      text,
    });
    note.save().then(res.json({
      'message': 'Success',
    }))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const getNoteById = async (req, res, next) => {
  try {
    const {id} = req.params;
    const note = await Note.find({userId: req.user.userId, _id: id}, '-__v');

    if (note.length > 0) {
      return res.json({
        note: note[0],
      });
    } else {
      return res.status(400).json({
        'message': 'Note not found',
      });
    }
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const updateNoteById = async (req, res, next) => {
  try {
    const {id} = req.params;
    const {text} = req.body;
    let note = await Note.find({userId: req.user.userId, _id: id}, '-__v');

    console.log(note);
    note = note[0];
    console.log(note);

    if (!note) {
      return res.status(400).json({
        'message': 'Note not found',
      });
    }
    if (!text) {
      return res.status(400).json({
        'message': 'Text field is required',
      });
    }

    note.text = text;
    note.save()
        .then(res.json({
          'message': 'Success',
        }))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const isCompletedNoteById = async (req, res, next) => {
  try {
    const {id} = req.params;

    return Note.findByIdAndUpdate({userId: req.user.userId, _id: id},
        {$set: {completed: true}})
        .then(res.json({
          'message': 'Success',
        }))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const deleteNoteById = async (req, res, next) => {
  try {
    const {id} = req.params;

    return Note.findByIdAndDelete({userId: req.user.userId, _id: id})
        .then(res.json({
          'message': 'Success',
        }))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};


module.exports = {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  isCompletedNoteById,
  deleteNoteById,
};
